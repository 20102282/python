from mpi4py import MPI 
import numpy
import numpy as np

comm = MPI.COMM_WORLD 
rank = comm.rank
size = comm.size

array_size = 3
recvdata = numpy.zeros(array_size, dtype=int)
senddata = (rank+1)*numpy.arange(array_size,dtype=int)

print("process %s sending %s " %(rank, senddata))
comm.Reduce (senddata,recvdata, root=0, op=MPI.SUM)
print('on task',rank,'after reduce: data = ',recvdata)