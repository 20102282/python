from mpi4py import MPI
comm = MPI.COMM_WORLD

size = comm.Get_size()
rank = comm.Get_rank()
data = (rank+1)**2
data = comm.gather(data, root=0)

if rank == 0:
    for i in range(1,size):
        assert data[i] == (i+1)**2
        value=data[i]
        print("rank: %s data:  %s" % (i, value))

else:
    assert data is None

