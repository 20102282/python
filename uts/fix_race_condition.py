# Import library yang dibutuhkan
import threading
import time

# Buat fungsi untuk menambahkan nilai shared variable
def add_value():
    # Dapatkan pelock
    lock.acquire()
    
    # Dapatkan nilai shared variable
    global shared_variable
    shared_variable += 1
    
    # Print nilai shared variable setiap kali dipanggil
    print("Nilai saat ini:", shared_variable)
    
    # Lepaskan pelock
    lock.release()

# Buat shared variable dengan nilai awal 0
shared_variable = 0

# Buat pelock
lock = threading.Lock()

# Buat sebanyak 10 thread
threads = [threading.Thread(target=add_value) for i in range(10)]

# Mulai setiap thread
for thread in threads:
    thread.start()

# Tunggu selama 1 detik
time.sleep(1)

# Print nilai shared variable setelah 1 detik
print("Nilai akhir:", shared_variable)
