from mpi4py import MPI

# Mendapatkan rank dan size dari proses yang sedang berjalan
rank = MPI.COMM_WORLD.rank
size = MPI.COMM_WORLD.size

# Memastikan bahwa hanya proses dengan rank 0 yang akan melakukan perhitungan
if rank == 0:
    # Melakukan perhitungan 2 pangkat n dimana nilai n berkisar dari 1 hingga 20
    result = [2**n for n in range(1, 21)]

    # Mengumpulkan hasil perhitungan pada proses dengan rank 0
    MPI.COMM_WORLD.gather(result, root=0)